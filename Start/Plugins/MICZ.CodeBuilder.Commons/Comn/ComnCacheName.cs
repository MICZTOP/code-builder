﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MICZ.CodeBuilder.Commons.Comn
{

    /// <summary>
    /// 通用缓存名字
    /// </summary>
    public class ComnCacheName
    {
        /// <summary>
        /// 用户类型
        /// </summary>
        public static string Cache_UserType = "Cache_UserType";

        /// <summary>
        /// 权限类型
        /// </summary>
        public static string Cache_PermissionType = "Cache_PermissionType";

        /// <summary>
        /// 平台类型
        /// </summary>
        public static string Cache_PlatformType = "Cache_PlatformType";

        /// <summary>
        /// 消息类型
        /// </summary>
        public static string Cache_MsgType = "Cache_MsgType";

        /// <summary>
        /// 用户状态
        /// </summary>
        public static string Cache_UserState = "Cache_UserState";


        /// <summary>
        ///临时验证码（Cache_ValidateCode）
        /// </summary>
        public static string Cache_ValidateCode = "Cache_ValidateCode";

        /// <summary>
        /// 后台 用户登录后保存的用户信息
        /// </summary>
        public static string Cache_ManagerCurrentUser = "Cache_ManagerCurrentUser";  
        
        /// <summary>
        /// 微信小程序登录后保存的用户信息
        /// </summary>
        public static string Cache_ManagerWeChatUser = "Cache_ManagerWeChatUser";

        /// <summary>
        /// 前台台 会员登录后保存的用户信息
        /// </summary>
        public static string Cache_MemberInfo = "Cache_MemberInfo";

        /// <summary>
        /// 行政区域树
        /// </summary>
        public static string Cache_Area = "Cache_Area";



        #region 前台

        /// <summary>
        /// 系统图书分类树形目录
        /// </summary>
        public static string Cache_Font_SystemBookClass = "Cache_Font_SystemBookClass";

        #endregion

        #region 后台

        #endregion

        #region 通用
        /// <summary>
        /// 国家分类
        /// </summary>
        public static string Cache_CountryBookClass = "Cache_CountryBookClass";
        
        /// <summary>
        /// 国家分类分页
        /// </summary>
        public static string Cache_CountryBookClass_Pages = "Cache_CountryBookClass_Pages";

        /// <summary>
        /// 查询地区图书馆
        /// </summary>
        public static string Cache_AreaLibraryList = "Cache_AreaLibraryList";

        /// <summary>
        /// 查询地区图书馆Total
        /// </summary>
        public static string Cache_AreaLibraryListTotal = "Cache_AreaLibraryListTotal";
        #endregion
    }



}
