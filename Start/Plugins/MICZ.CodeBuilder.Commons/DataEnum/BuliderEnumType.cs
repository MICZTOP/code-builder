﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MICZ.CodeBuilder.Commons.DataEnum
{
    public class BuliderDataEnum
    {
        public enum DataEnum
        {
            [Description("服务层接口")]
            IService = 1,
            [Description("服务层")]
            Service = 2,
            [Description("实体模型")]
            Model = 3,
            [Description("视图模型")]
            ViewModel = 4,
            [Description("仓库接口")]
            IRepository = 5,
            [Description("仓库")]
            Repository = 6,
            [Description("App层接口")]
            IApplocation = 7,
            [Description("App层")]
            Applocation = 8,
            [Description("控制器")]
            Controller = 9,
            [Description("注册层")]
            Register = 10,
            [Description("AutoMapper")]
            AutoMapper=11
        }
    }
}
