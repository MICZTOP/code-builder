﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace MICZ.CodeBuilder.Commons.ServerTypeToModelType
{
    public static class ServerTypeToModelType
    {
        //private static string[] _string = new string[] { "char", "nchar", "varchar", "nvarchar" };
        //private static string[] _int = new string[] { "int", "smallint" };
        //private static string[] _long = new string[] { "bigint" };
        //private static string[] _DateTime = new string[] { "datetime", "smalldatetime" };
        //private static string[] _float = new string[] { "float" };
        //private static string[] _bool = new string[] { "byte" };
        //private static string[] _decimal = new string[] { "decimal", "money" };
        private static List<string> _string = new List<string> { "text","char", "nchar", "varchar", "nvarchar" };
        private static List<string> _int = new List<string> { "integer", "int", "smallint" };
        private static List<string> _long = new List<string> { "bigint" };
        private static List<string> _DateTime = new List<string> { "date", "datetime", "smalldatetime" };
        private static List<string> _float = new List<string> { "float" };
        private static List<string> _bool = new List<string> { "byte","bit" };
        private static List<string> _decimal = new List<string> { "decimal", "money" };

        /// <summary>
        /// 数据库类型转换为实体类型
        /// </summary>
        /// <param name="ServerType"></param>
        /// <returns></returns>
        public static string ServerConverModel(string ServerType)
        {
            ServerType = ServerType.ToLower();
            if (_string.Contains(ServerType))
                return "string";
            else if (_int.Contains(ServerType))
                return "int";
            else if (_long.Contains(ServerType))
                return "long";
            else if (_DateTime.Contains(ServerType))
                return "DateTime";
            else if (_float.Contains(ServerType))
                return "float";
            else if (_bool.Contains(ServerType))
                return "bool";
            else if (_decimal.Contains(ServerType))
                return "decimal";
            else
                return ServerType;
        }

        /// <summary>
        /// 数据库类型转换为C#类型
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        private static Type GetType(string dataType)
        {
            switch (dataType.ToLower())
            {
                case "nvarchar":
                case "varchar":
                case "nchar":
                case "char":
                    return typeof(string);
                case "int":
                    return typeof(int);
                case "bigint":
                    return typeof(long);
                case "bit":
                    return typeof(bool);
                case "datetime":
                    return typeof(DateTime);
                default:
                    return typeof(object);
            }

        }

        private static string Get(string dataType)
        {

            switch (dataType.ToLower())
            {
                case "system.string":
                    return "ToString";
                case "system.int32":
                    return "ToInt32";
                case "system.int64":
                    return "ToInt64";
                case "system.datetime":
                    return "ToDateTime";
                case "system.boolean":
                    return "ToBoolean";

                default:
                    throw new Exception("找不到匹配的数据类型");

            }
        }
    }

}
