﻿using System;
using System.Collections.Generic;
using System.Text;
using Yitter.IdGenerator;

namespace MICZ.CodeBuilder.Commons.DistributedID
{
    //Yitter.IdGenerator
    //雪花漂移ID
    public class IdGenerator:YitIdHelper
    {
        IdGeneratorOptions IdGeneratorOptions;
        public IdGenerator()
        {
            if (IdGeneratorOptions != null) {
                var IdGeneratorConf = ConfigExtensions.ConfigExtensions.Configuration.GetSection("IdGenerator");
                IdGeneratorOptions.WorkerId =ushort.Parse(IdGeneratorConf.GetSection("WorkerId").Value);
                IdGeneratorOptions.WorkerIdBitLength = byte.Parse(IdGeneratorConf.GetSection("WorkerIdBitLength").Value);
            }
        }
    }
}
