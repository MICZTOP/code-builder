﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MICZ.CodeBuilder.Commons.DistributedID
{


    /// <summary>
    /// 通用调用 获取分布式id方法
    /// </summary>
    public class DistributedID
    {
        static readonly UniqueSequenceGUID UniqueId = new UniqueSequenceGUID();

        #region 方法1 生成类似Mongodb的ObjectId有序、不重复Guid
        /// <summary>
        /// 生成类似Mongodb的ObjectId有序、不重复Guid
        /// </summary>
        /// <returns></returns>
        public static Guid NewMongodbId()
        {
            return MongodbGUID.NewMongodbId();
        }


        #endregion

        #region 方法2 生成基于时间的id

        /// <summary>
        /// 生成一个唯一的更加有序的GUID形式的长整数,在一秒内，重复概率低于 千万分之一，线程安全。可用于严格有序增长的ID
        /// </summary>
        /// <returns></returns>
        public static long NewUniqueSequenceGUID()
        {
            return UniqueId.NewID();
        }

        /// <summary>
        /// 生成一个新的在秒级别有序的长整形“GUID”，在一秒内，数据比较随机，线程安全，
        /// 但不如NewUniqueSequenceGUID 方法结果更有序（不包含毫秒部分）
        /// </summary>
        /// <returns></returns>
        public static long NewSequenceGUID()
        {
            return UniqueSequenceGUID.InnerNewSequenceGUID(DateTime.Now, false);
        }

     

        /// <summary>
        /// 当前机器ID，可以作为分布式ID，如果需要指定此ID，请在应用程序配置文件配置 SOD_MachineID 的值，范围大于100，小于1000.
        /// </summary>
        /// <returns></returns>
        public static int CurrentMachineID()
        {
            return UniqueSequenceGUID.GetCurrentMachineID();
        }
        #endregion
    }
}
