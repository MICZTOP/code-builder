﻿using MICZ.CodeBuilder.Commons.DataEnum;
using System;
using System.Collections.Generic;
using System.Text;

namespace MICZ.CodeBuilder.ViewModels.PageData
{
    public class PostData
    {
       public DataEnumType.DataEnum Type { get; set; }
       public  string Obj { get; set; }
    }
}
