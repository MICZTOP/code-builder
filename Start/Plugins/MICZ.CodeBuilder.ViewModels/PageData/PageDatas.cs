﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MICZ.CodeBuilder.ViewModels.PageData
{
    public class PageDatas<T>
    {
        public int Page { get; set; }
        public int Rows { get; set; }
        public int TotalCount { get; set; }
        public List<T> DataList { get; set; }
    }
}
