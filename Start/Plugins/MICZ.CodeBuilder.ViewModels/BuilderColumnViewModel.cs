﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MICZ.CodeBuilder.ViewModels
{
    /// <summary>
    /// 模板替换对象
    /// </summary>
    public class BuilderColumnViewModel
    {
        /// <summary>
        /// 命名空间
        /// </summary>
        public string Namespace { get; set; }
        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 列是否为主键
        /// </summary>
        public bool ColumnIsPrimarykey { get; set; } 
        /// <summary>
        /// 列是否为自增
        /// </summary>
        public bool ColumnIsIdentity { get; set; } 
        /// <summary>
        /// 列名
        /// </summary>
        public string ColumnName { get; set; } 
        /// <summary>
        /// 类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 表注释
        /// </summary>
        public string Description { get; set; }
    }
}
