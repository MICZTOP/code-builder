﻿/***************************************************
* 模块名称：通用枚举类
* 功能概述：通用枚举类
*************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace MICZ.CodeBuilder.ViewModels.Enums
{


    #region 角色 权限状态

    /// <summary>
    /// 在页面中查询时 权限类型（0页面，2通用API）
    /// </summary>
    public enum ViewModelPermissionType
    {
        /// <summary>
        /// 页面
        /// </summary>
        [Description("页面")] Page = 0,

        /// <summary>
        /// 通用API
        /// </summary>
        [Description("通用API")] API = 2
    }


    /// <summary>
    /// 权限类型（0页面，1按钮，2通用API）
    /// </summary>
    public enum PermissionType
    {
        /// <summary>
        /// 页面
        /// </summary>
        [Description("页面")] Page = 0,

        /// <summary>
        /// 按钮
        /// </summary>
        [Description("按钮")] Button = 1,

        /// <summary>
        /// 通用API
        /// </summary>
        [Description("通用API")] API = 2
    }

    /// <summary>
    /// 菜单类型（1后端菜单，2前端菜单）
    /// </summary>
    public enum FrontBackMenuType
    {
        /// <summary>
        /// 后端菜单
        /// </summary>
        [Description("后端菜单")]
        BackMenu = 1,

        /// <summary>
        /// 前端菜单
        /// </summary>
        [Description("前端菜单")]
        FrontMenu=2


    }


    /// <summary>
    /// 前端框架组件（UserLayout, BasicLayout, RouteView, BlankLayout, PageView）
    /// </summary>
    public enum Component
    {
        /// <summary>
        /// UserLayout
        /// </summary>
        [Description("UserLayout")] UserLayout = 0,

        /// <summary>
        /// BasicLayout
        /// </summary>
        [Description("BasicLayout")] BasicLayout = 1,

        /// <summary>
        /// RouteView
        /// </summary>
        [Description("RouteView")] RouteView = 2,

        /// <summary>
        /// BlankLayout
        /// </summary>
        [Description("BlankLayout")] BlankLayout = 3,

        /// <summary>
        /// PageView
        /// </summary>
        [Description("PageView")] PageView = 4,

    }


    #endregion



    /// <summary>
    /// 登录方式
    /// </summary>
    public enum LoginType
    {
        /// <summary>
        /// 密码登录
        /// </summary>
        [Description("密码登录")] Pwd=0,

        /// <summary>
        /// 发送手机验证码
        /// </summary>
        [Description("发送手机验证码")] MobileCode=1,

        /// <summary>
        /// 扫码
        /// </summary>
        [Description("扫码")] SweepCode=2,
    }




    /// <summary>
    /// 平台终端类型(0暂无 1PC网页端 2Android 3IOS 4PC客户端 5其他)
    /// </summary>
    public enum PlatformType
    {
        [Description("暂无")] NO = 0,

        [Description("PC网页端")] PCWeb = 1,

        [Description("Android")] Android = 2,

        [Description("IOS")] IOS = 3,

        [Description("PC客户端")] PCClient = 4,

        [Description("其他")] Other = 5,

    }

    /// <summary>
    /// 消息类型
    /// </summary>
    public enum MsgType
    {
        [Description("手机号码")] Mobile = 1,

        [Description("电子邮箱")] Email = 2,

        [Description("站内推送")] Station = 3,

        [Description("其他")] Other = 4

    }

    /// <summary>
    /// 用户状态
    /// </summary>
    public enum UserState
    {
        /// <summary>
        /// 正常
        /// </summary>
        [Description("正常")] Normal = 1,

        /// <summary>
        /// 锁定
        /// </summary>
        [Description("锁定")] Locking = 0,

        /// <summary>
        /// 封存
        /// </summary>
        [Description("封存")] SealUp = -1
    }

    /// <summary>
    /// 用户登录状态
    /// </summary>
    public enum LoginState
    {
        [Description("登录失败")] Fail = 0,

        [Description("登录成功")] Success = 1
    }


    /// <summary>
    /// 用户类型(0一般用户或游客,1机构或图书馆,2供应商,3系统,4书店)
    /// </summary>
    public enum UserType
    {
        /// <summary>
        /// 一般用户或游客
        /// </summary>
        [Description("一般用户或游客")] GeneralUser = 0,


        /// <summary>
        /// 系统
        /// </summary>
        [Description("系统")] System = 3,

        /// <summary>
        /// 机构或公共图书馆
        /// </summary>
        [Description("机构图书馆")] OrganLibrary = 1,

        /// <summary>
        /// 供应商
        /// </summary>
        [Description("供应商")] Supplier = 2,

      

        /// <summary>
        /// 书店
        /// </summary>
        [Description("书店")] BookStore =4,

    }

    /// <summary>
    /// 状态
    /// </summary>
    public enum Status
    {
        /// <summary>
        /// 已申请
        /// </summary>
        [Description("已申请")] AlreadyApplied = 0,

        /// <summary>
        /// 申请通过
        /// </summary>
        [Description("申请通过")] ApplicationPassed = 1,

        /// <summary>
        /// 申请不通过
        /// </summary>
        [Description("申请不通过")] ApplicationFailed = 2
    }

    /// <summary>
    /// 类型
    /// </summary>
    public enum SupplyType
    {
        /// <summary>
        /// 出版社
        /// </summary>
        [Description("出版社")] PublishingHouse = 1,

        /// <summary>
        /// 图书公司
        /// </summary>
        [Description("图书公司")] BookCompany = 2,

        /// <summary>
        /// 书店
        /// </summary>
        [Description("书店")] Bookstore = 3,

        /// <summary>
        /// 图书馆
        /// </summary>
        [Description("图书馆")] Library = 4
    }

    /// <summary>
    /// 操作类型
    /// </summary>
    public enum OperatType
    {
        /// <summary>
        /// 增加
        /// </summary>
        [Description("增加")] Insert = 1,

        /// <summary>
        /// 删除
        /// </summary>
        [Description("删除")] Delete = 2,

        /// <summary>
        /// 修改
        /// </summary>
        [Description("修改")] Update = 3,

        /// <summary>
        /// 查询
        /// </summary>
        [Description("查询")] Inquiry = 4
    }

    /// <summary>
    /// 经营状态
    /// </summary>
    public enum ManageState
    {
        /// <summary>
        /// 在营
        /// </summary>
        [Description("在营")] Business = 0,

        /// <summary>
        /// 开业
        /// </summary>
        [Description("开业")] Opening = 1,

        /// <summary>
        /// 在册
        /// </summary>
        [Description("在册")] Registered = 2
    }

    /// <summary>
    /// 文章状态
    /// </summary>
    public enum NewsStatus
    {
        /// <summary>
        /// 草稿
        /// </summary>
        [Description("草稿")] AlreadyApplied = 0,

        /// <summary>
        /// 已发布
        /// </summary>
        [Description("已发布")] ApplicationPassed = 1
    }
}
