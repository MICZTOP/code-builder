﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MICZ.CodeBuilder.ViewModels
{
    /// <summary>
    /// 模板替换对象
    /// </summary>
    public class BuilderTableViewModel
    {
        /// <summary>
        /// 命名空间
        /// </summary>
        public string Namespace { get; set; }
        
        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 表注释
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 表字段列表
        /// </summary>
        public List<BuilderColumnViewModel> Columns { get; set; }
    }
}
