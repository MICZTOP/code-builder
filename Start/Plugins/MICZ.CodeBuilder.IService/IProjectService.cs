﻿using MICZ.CodeBuilder.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MICZ.CodeBuilder.IService
{
    /// <summary>
    /// 项目服务层
    /// </summary>
    public interface IProjectService
    {
        #region 项目
        /// <summary>
        /// 模板创建
        /// </summary>
        /// <param name="TemplateFileName">模板名称(Controller)</param>
        /// <param name="obj">生成对象</param>
        /// <returns></returns>
        Task<string> ControllerTemplateCode(string TemplateFileName, BuilderTableViewModel obj);
        /// <summary>
        /// 生成代码保存到CodeBuilder
        /// </summary>
        /// <param name="Projectname">项目命名空间名称</param>
        /// <param name="TemplateFileName">模板名称(Controller)</param>
        /// <param name="obj">生成对象</param>
        /// <returns></returns>
        void ControllerTemplateCreate(string TemplateFileName, BuilderTableViewModel obj);  
        
        /// <summary>
        /// 模板创建
        /// </summary>
        /// <param name="TemplateFileName">模板名称(Service,AppService)</param>
        /// <param name="obj">生成对象</param>
        /// <returns></returns>
        Task<string> TemplateCode(string TemplateFileName, BuilderTableViewModel obj);
        /// <summary>
        /// 生成代码保存到CodeBuilder
        /// </summary>
        /// <param name="Projectname">项目命名空间名称</param>
        /// <param name="TemplateFileName">模板名称(Service,AppService)</param>
        /// <param name="obj">生成对象</param>
        /// <returns></returns>
        void TemplateCreate(string TemplateFileName, BuilderTableViewModel obj);

        /// <summary>
        /// Repository模板创建
        /// </summary>
        /// <param name="TemplateFileName">模板名称(Repository)</param>
        /// <param name="obj">生成对象</param>
        /// <returns></returns>
        Task<string> RepositoryTemplateCode(string TemplateFileName, List<BuilderTableViewModel> obj);
        /// <summary>
        /// Repository层生成代码保存到CodeBuilder
        /// </summary>
        /// <param name="Projectname">项目命名空间名称</param>
        /// <param name="TemplateFileName">模板名称(Repository)</param>
        /// <param name="obj">生成对象</param>
        /// <returns></returns>
        void RepositoryTemplateCreate(string TemplateFileName, List<BuilderTableViewModel> obj);

        /// <summary>
        /// 生成Model
        /// </summary>
        /// <param name="Projectname">项目命名空间名称</param>
        /// <param name="obj">生成对象(列表)</param>
        /// <param name="Type">Model类型:1Model2ViewModel</param>
        /// <returns></returns>
        Task<string> TemplateModel(string Projectname, List<BuilderColumnViewModel> obj, int Type = 1);

        /// <summary>
        /// 生成Model保存到CodeBuilder
        /// </summary>
        /// <param name="Projectname">项目命名空间名称</param>
        /// <param name="Type">Model类型:1Model2ViewModel</param>
        /// <returns></returns>
        void ModelCreate(string Projectname, int Type = 1);

        /// <summary>
        /// 创建WebApi项目(.csproj)自动添加到sln解决方案
        /// </summary>
        /// <param name="name">项目名称</param>
        void WebApiCreateProject(string name); 
        
        /// <summary>
        /// 创建项目(.csproj)自动添加到sln解决方案
        /// </summary>
        /// <param name="name">项目名称</param>
        void CreateProject(string name);

        /// <summary>
        /// 添加项目引用到解决方案(.sln)
        /// </summary>
        /// <param name="projectId">项目id</param>
        /// <param name="projectName">项目名称</param>
        void AppendProjectToSln(string projectId, string projectName);

        /// <summary>
        /// 修改解决方案
        /// </summary>
        /// <param name="solutionName">解决方案名称</param>
        void RenameSln(string solutionName);
        #endregion
    }
}
