﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MICZ.CodeBuilder.Commons.RandomExtensions;
using MICZ.CodeBuilder.Commons.VerifyCode;

namespace MICZ.CodeBuilder.ManageApi.Controllers
{
    public class VerifyCodeController : BaseController
    {
        /// <summary>
        /// Base64验证码
        /// </summary>
        /// <returns></returns>
        public IActionResult Base64()
        {
            string base64 = "data:image/png;base64," + Encoding.Default.GetString(DarwingVerifyCode());
            return Content(base64);
        }

        /// <summary>
        /// Img格式验证码
        /// </summary>
        /// <returns></returns>
        public IActionResult Img()
        {
            byte[] bytes = DarwingVerifyCode();
            return new FileContentResult(bytes, "image/png"); ;
        }

        /// <summary>
        /// Byte格式验证码(内部调用)
        /// </summary>
        /// <returns></returns>
        private byte[] DarwingVerifyCode()
        {
            string verifyCode = RandomExtensions.GuidString(4);
            MICZ.CodeBuilder.Commons.SessionExtensions.SessionExtensions.SetString(this.HttpContext, "VerifyCode", verifyCode);
            return VerifyCode.DarwingVerifyCode(verifyCode);
        }
    }
}
