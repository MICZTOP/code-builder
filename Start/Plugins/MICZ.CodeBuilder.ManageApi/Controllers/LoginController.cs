﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MICZ.CodeBuilder.Commons.JwtExtensions;
using MICZ.CodeBuilder.Commons.SessionExtensions;
using MICZ.CodeBuilder.IService.Userinfo;
using MICZ.CodeBuilder.Models;

namespace MICZ.CodeBuilder.ManageApi.Controllers
{
    public class LoginController : BaseController
    {
        IUserinfoService userinfoService;
        public LoginController(IUserinfoService _userinfoService)
        {
            userinfoService = _userinfoService;
        }

        [EnableCors]
        public JsonResult Ok(string uid,string password, string vcode)
        {
            if (vcode != MICZ.CodeBuilder.Commons.SessionExtensions.SessionExtensions.GetString(this.HttpContext, "VerifyCode"))
            {
                JsonDataModel.Status = 0;
                JsonDataModel.Msg = "数据请求失败！验证码错误";
                return new JsonResult(JsonDataModel);
            }
            if (string.IsNullOrEmpty(uid) || string.IsNullOrEmpty(password))
            {
                JsonDataModel.Status = 0;
                JsonDataModel.Msg = "数据请求失败！请检查用户名及其密码";
                return new JsonResult(JsonDataModel);
            }
            Userinfo userinfo =userinfoService.Get(a => a.USERID == uid);//&&a.Password==admins.Password&&a.State==1
            if (userinfo == null)
            {
                JsonDataModel.Status = 0;
                JsonDataModel.Msg = "数据请求失败！请检查用户名及其密码";
                return new JsonResult(JsonDataModel);
            }
            else if (userinfo.PASSWORD != password)
            {
                JsonDataModel.Status = 0;
                JsonDataModel.Msg = "用户名及其密码错误";
                return new JsonResult(JsonDataModel);
            }
            else if (userinfo.ISSTATE != 1)
            {
                JsonDataModel.Status = 0;
                JsonDataModel.Msg = "用户未启用";
                return new JsonResult(JsonDataModel);
            }
            dynamic obj = new MICZ.CodeBuilder.Commons.DynamicExtensions.DynamicObjectHelper();
            var JWT = JwtHelper.CreateJWT(userinfo.USERID, userinfo.ID.ToString());
            obj.set("JWT", JWT);
            obj.set("Src", "/Manage/Home/Index");
            JsonDataModel.Status = 1;
            JsonDataModel.Msg = "登录成功";
            JsonDataModel.Data = obj;
            return new JsonResult(JsonDataModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID">用户名</param>
        /// <param name="Password">密码</param>
        /// <param name="PWD">确认密码</param>
        /// <param name="Role">角色</param>
        /// <param name="vcode">验证码</param>
        /// <returns></returns>
        //public JsonResult AddUser(string UID, string Password, string PWD, string Role, string vcode)
        //{
        //    if (vcode != this.HttpContext.Session.GetString("VerifyCode"))
        //    {
        //        JsonDataModel.Status = 0;
        //        JsonDataModel.Msg = "数据请求失败！验证码错误";
        //        return new JsonResult(JsonDataModel);
        //    }
        //    if (Password != PWD)
        //    {
        //        JsonDataModel.Status = 0;
        //        JsonDataModel.Msg = "两次输入的密码不一致，请重新输入";
        //        return new JsonResult(JsonDataModel);
        //    }
        //    SqlHelp sqlHelp = new SqlHelp();
        //    dynamic dataObj = new FY.DLL.Command.DynamicObjectHelper();
        //    #region 设置Id
        //    int Id = 0;
        //    bool IsId = false;
        //    do
        //    {
        //        Id = RandomExtensions.GuidInt(5);
        //        if (sqlHelp.AdminsDb.GetTop(a => a.ID == Id) == null)
        //        {
        //            IsId = true;
        //        }
        //    }
        //    while (IsId != true);
        //    #endregion
        //    Admins admins = new Admins();
        //    admins.ID = Id;
        //    admins.UID = UID;
        //    admins.Password = Password;
        //    List<FY.DLL.Models.Auth_Users> auth_ = sqlHelp.Auth_UsersDb.GetLists(a => a.Name == Role.Trim());
        //    if (auth_.Count > 0)
        //        admins.Auth_ID = auth_[0].ID;
        //    admins.State = 1;
        //    admins.Create_Time = DateTime.Now;
        //    int s = sqlHelp.AdminsDb.InserEntity(admins);
        //    if (s > 0)
        //    {
        //        dataObj.set("Tree", admins);
        //        JsonDataModel.Status = 1;
        //        JsonDataModel.Msg = "数据修改成功！";
        //        JsonDataModel.Data = dataObj;
        //    }
        //    else
        //    {
        //        JsonDataModel.Status = 0;
        //        JsonDataModel.Msg = "数据修改失败！";
        //    }
        //    //JsonDataModel.Data = dataObj;
        //    return new JsonResult(JsonDataModel);
        //}

    }
}
