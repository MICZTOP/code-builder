﻿using Microsoft.Extensions.DependencyInjection;
using MICZ.CodeBuilder.IService;
using MICZ.CodeBuilder.Service.Project;
using System;
using System.Collections.Generic;
using System.Text;

namespace MICZ.CodeBuilder.Service.Register
{
    public static class RegisterService
    {
        public static void Register(IServiceCollection services)
        {
            services.AddTransient<IProjectService, ProjectService>();
        }
        public static IServiceCollection AddServiceRegister(this IServiceCollection services)
        {
            services.AddTransient<IProjectService, ProjectService>();
            return services;
        }
    }
}
