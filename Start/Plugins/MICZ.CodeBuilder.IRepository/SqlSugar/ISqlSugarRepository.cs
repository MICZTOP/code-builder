﻿using SqlSugar;

namespace MICZ.CodeBuilder.IRepository.SqlSugar
{
    public interface ISqlSugarRepository
    {

        public SqlSugarClient Db { get; }
        //public IDbContextRepository<Userinfo> UserinfoDb();
    }
}
