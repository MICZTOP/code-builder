﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using MICZ.CodeBuilder.ViewModels;

namespace MICZ.CodeBuilder.Manage.Controllers
{
    /// <summary>
    /// 控制器父类重写定义
    /// </summary>
    [Area("Manage")]
    public class BaseController : Controller
    {
        //工作上下文
        public WorkContent WorkContent = new WorkContent();
        //返回数据对象
        public JsonDataModel JsonDataModel = new JsonDataModel();
        /// <summary>
        /// Action执行之前
        /// </summary>
        //public override void OnActionExecuting(ActionExecutingContext filterContext)
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            if (context.RouteData.Values["area"] != null)
                WorkContent.AreasName = context.RouteData.Values["area"].ToString();
            WorkContent.ControllerName = context.RouteData.Values["controller"].ToString();//.GetRouteData(this.HttpContext).Values["controller"].ToString();
            WorkContent.ActionName = context.RouteData.Values["action"].ToString();
            //开始请求时间
            WorkContent.StartExecuteTime = DateTime.Now;
            WorkContent.CurrentPath = Directory.GetCurrentDirectory();
            //WorkContent.ThemeAPI = string.Format("/{0}/StaticFiles", "API");
            WorkContent.ThemePath = string.Format("/{0}", WorkContent.AreasName);
            ViewBag.WorkContent = WorkContent;
            //if (authorization == null || authorization.Scheme != "Bearer") return;
            //var token = authorization.Parameter; // 获取token字符串
        }

        /// <summary>
        /// Action执行之后
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);
            WorkContent.EndExecuteTime = DateTime.Now.Subtract(WorkContent.StartExecuteTime).TotalMilliseconds / 1000;
        }
    }
}
