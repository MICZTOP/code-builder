﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace MICZ.CodeBuilder.Manage.Controllers
{
    public class LoginController : BaseController
    {
        [EnableCors]
        public IActionResult Index()
        {
            ViewData["title"] = "登陆页";
            return View();
        }


        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="UID">用户名</param>
        /// <param name="Password">密码</param>
        /// <param name="PWD">确认密码</param>
        /// <param name="Role">角色</param>
        /// <param name="vcode">验证码</param>
        /// <returns></returns>
        [EnableCors]
        public IActionResult AddUser()
        {
            ViewData["title"] = "用户注册";
            return View();
        }
    }
}