using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MICZ.CodeBuilder.WebApi
{
    public class Startup
    {
        private IConfiguration Configuration;
        string Cores = "cors";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddSingleton<>(IConfiguration);
            #region 跨域
            //var urls = Configuration["AppHost:Cores"].Split(',');
            //var Configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json", true,true).Build();
            var urls = Configuration.GetSection("AppHost").GetSection("Cores").Value.Split(',');

            services.AddCors(options =>
            {
                options.AddPolicy(Cores,
                    builder =>
                    {
                        //builder.WithOrigins("https://localhost:44390", "http://0.0.0.0:3201").AllowAnyHeader();
                        builder.WithOrigins(urls) // 允许部分站点跨域请求
                                                  //.AllowAnyOrigin() // 允许所有站点跨域请求（net core2.2版本后将不适用）
                                .AllowAnyMethod() // 允许所有请求方法
                                .AllowAnyHeader() // 允许所有请求头
                                .AllowCredentials(); // 允许Cookie信息
                    });
            });

            #endregion

            //services.AddMvc();
            //注册TimedJob定时任务中间件
            //services.AddTimedJob();
            //视图动态刷新不需要重启项目
            //services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddDistributedMemoryCache().AddSession(o =>
            {
                o.IdleTimeout = TimeSpan.FromHours(24);
            });
            services.AddMvc().AddRazorRuntimeCompilation().AddNewtonsoftJson(options =>
            {
                // 忽略循环引用
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                // 不使用驼峰
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                // 设置时间格式
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                // 如字段为null值，该字段不会返回到前端
                // options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });
            services.AddRazorPages();
            //自定义MVC视图路径
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.AreaViewLocationFormats.Clear();

                options.AreaViewLocationFormats.Add("/Views/{1}/{0}.cshtml");
                options.AreaViewLocationFormats.Add("/Views/Shared/{0}.cshtml");

                //基础配置
                options.AreaViewLocationFormats.Add("/Views/Shared/{0}.cshtml");
            });
            //services.AddCors(option => option.AddPolicy("cors", policy => policy.AllowAnyHeader().AllowAnyMethod().AllowCredentials().WithOrigins(new[] {"http://Localhost:80", "http://127.0.0.1:80","http://test.micz.top:80" })));
            #region 依赖注入
            //            AddSingleton的生命周期：

            //项目启动 - 项目关闭   相当于静态类 只会有一个

            //AddScoped的生命周期：

            //请求开始 - 请求结束  在这次请求中获取的对象都是同一个

            //  AddTransient的生命周期：

            //请求获取 -（GC回收 - 主动释放） 每一次获取的对象都不是同一个
            //services.AddSingleton<IRepository,Repository>();//依赖注入生命周期永恒不变
            //services.AddScoped<IRepository,Repository>();//依赖注入生命周期每个页面不变
            //services.AddTransient<IRepository,Repository>();//依赖注入生命周期每次请求都不一样
            //Base.Repository(services);
            //Base.ServiceRepository(services);
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //静态文件支持
            app.UseStaticFiles();
            //使用TimedJob定时任务
            //app.UseTimedJob();
            //服务器静态文件wwwroot
            //app.UseFileServer();
            app.UseStaticFiles(new StaticFileOptions
            {
                //1.配置路径
                //FileProvider = new PhysicalFileProvider(new DirectoryInfo(Directory.GetCurrentDirectory()).ToString())
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"Plugins")),
                //2.配置可以被请求到的路径
                //RequestPath = "/StaticFiles"
                //RequestPath = new PathString("/Content")
            });
            //Session
            app.UseSession();
            app.UseRouting();
            app.UseCors(Cores);
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "Default",
                    pattern: "WebApi/{controller=Home}/{action=Index}"
                    );
                endpoints.MapAreaControllerRoute(
                    name: "Area_Default",
                    areaName: "Area_Default",
                    pattern: "{area=WebApi}/{controller=Home}/{action=Index}"
                    );
                endpoints.MapRazorPages();
            });
        }
    } 
}
