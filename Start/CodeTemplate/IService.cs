﻿@("/***************************************************")
@("* 模块名称：" + Model.TableName + "IService")
@("* 功能概述：")
@("* 创建人：何贵祥")
@("* 创建时间：" + DateTime.Now.ToString("d") + "")
@("* 修改人：")
@("* 修改时间：")
@("* 修改描述：")
@("*************************************************/")
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Text;
using System.Linq;
using @(Model.Namespace).Model;
@using System.Collections.Generic;
@using MICZ.CodeBuilder.ViewModels;

namespace @(Model.Namespace).IService
{
    @{ List<BuilderColumnViewModel> _Columns = Model.Columns; bool IsDel = _Columns.Where(a => a.ColumnName.ToLower() == "isdel").Count() > 0 ? true : false; }
    public interface I@(Model.TableName)Service
    {
        
        #region 系统自动生成
        #region 获取
        @("/// <summary>")
        @("/// 获取一条数据")
        @("/// </summary>")
        @("/// <param name=\"expression\">树形表达式</param>")
        @("/// <param name=\"select\">查询列</param>")
        @("/// <param name=\"IsAll\">是否全局搜索(包含已删除)</param>")
        @("/// <returns></returns>")
        Task <@(Model.TableName)> Get(Expression< Func < @(Model.TableName), bool>> expression, string select = "*", bool IsAll = false);

        @("/// <summary>")
        @("/// 获取多条数据")
        @("/// </summary>")
        @("/// <param name=\"page\">页面(page=0,rows=0则显示全部)</param>")
        @("/// <param name=\"rows\">页面条数(page=0,rows=0则显示全部)</param>")
        @("/// <param name=\"OrderBy\">排序</param>")
        @("/// <param name=\"expression\">树形表达式</param>")
        @("/// <param name=\"select\">查询列</param>")
        @("/// <param name=\"IsAll\">是否全局搜索(包含已删除)</param>")
        @("/// <returns></returns>")
        Task<List< @(Model.TableName)>> GetList(int page, int rows, string OrderBy, Expression< Func < @(Model.TableName), bool>> expression, string select = "*", bool IsAll = false);

        @("/// <summary>")
        @("/// 获取数据总数")
        @("/// </summary>")
        @("/// <param name=\"expression\">树形表达式</param>")
        @("/// <param name=\"IsAll\">是否全局搜索(包含已删除)</param>")
        @("/// <returns></returns>")
        Task<long> GetListTotal(Expression<Func< @(Model.TableName), bool>> expression, bool IsAll = false);
        #endregion

        #region 提交插入
        @("/// <summary>")
        @("/// 创建数据")
        @("/// </summary>")
        @("/// <param name=\"model\">实体</param>")
        @("/// <param name=\"expression\">过滤对象a=>new {a.ID}</param>")
        @("/// <returns></returns>")
        Task<long> Create(@(Model.TableName) model, Expression<Func< @(Model.TableName), object>> obj = null); 
    
        @("/// <summary>")
        @("/// 创建数据")
        @("/// </summary>")
        @("/// <param name=\"model\">List<T>实体</param>")
        @("/// <param name=\"expression\">过滤对象a=>new {a.ID}</param>")
        @("/// <returns></returns>")
        Task<long> Create(List< @(Model.TableName) > model, Expression <Func< @(Model.TableName), object>> obj = null);
        #endregion

        #region 更新
        @("/// <summary>")
        @("/// 更新数据")
        @("/// </summary>")
        @("/// <param name=\"model\">实体</param>")
        @("/// <returns></returns>")
        Task<int> Update(@(Model.TableName) model);

        @("/// <summary>")
        @("/// 更新数据")
        @("/// </summary>")
        @("/// <param name=\"model\">实体</param>")
        @("/// <returns></returns>")
        Task<int> Update(@(Model.TableName) model, Expression< Func < @(Model.TableName), bool>> expression);

        @("/// <summary>")
        @("/// 更新数据")
        @("/// </summary>")
        @("/// <param name=\"model\">实体</param>")
        @("/// <returns></returns>")
        Task<int> Update(@(Model.TableName) model,string[] cols, Expression< Func < @(Model.TableName), bool>> expression);
        
        @("/// <summary>")
        @("/// 更新数据")
        @("/// </summary>")
        @("/// <param name=\"model\">实体</param>")
        @("/// <returns></returns>")
        Task<int> Update(@(Model.TableName) model, Expression< Func < @(Model.TableName), object>> obj, Expression< Func < @(Model.TableName), bool>> expression);
        #endregion

        #region 删除
        @if(IsDel)
        {
            @("/// <summary>")@:
            @("/// 逻辑删除")@:
            @("/// </summary>")@:
            @("/// <param name=\"expression\">树形表达式</param>")@:
            @("/// <returns></returns>")@:
            @:Task<int> SoftRemove(Expression< Func < @(Model.TableName), bool>> expression);

            @("/// <summary>")@:
            @("/// 恢复逻辑删除")@:
            @("///</summary>")@:
            @("/// <param name=\"expression\">树形表达式</param>")@:
            @("/// <returns></returns>")@:
            @:Task<int> CancelSoftRemove(Expression< Func < @(Model.TableName), bool>> expression);
        }

        @("/// <summary>")
        @("/// 实体删除")
        @("/// </summary>")
        @("/// <param name=\"model\">实体</param>")
        @("/// <returns></returns>")
        Task<int> Remove(Expression< Func < @(Model.TableName), bool>> expression);
        #endregion
        #endregion
    }


}
