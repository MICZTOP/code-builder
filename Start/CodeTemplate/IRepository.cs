﻿@("/***************************************************")
@("* 模块名称：IRepository")
@("* 功能概述：")
@("* 创建人：何贵祥")
@("* 创建时间：" + DateTime.Now.ToString("d") + "")
@("* 修改人：")
@("* 修改时间：")
@("* 修改描述：")
@("*************************************************/")
//using @(Model[0].Namespace).Model;
using SqlSugar;

namespace @(Model[0].Namespace).IRepository
{
    public interface ISqlSugarRepository
    {

        public SqlSugarClient Db { get; }

    }
}
