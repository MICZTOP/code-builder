﻿@("/***************************************************")
@("* 模块名称：Register")
@("* 功能概述：")
@("* 创建人：何贵祥")
@("* 创建时间：" + DateTime.Now.ToString("d") + "")
@("* 修改人：")
@("* 修改时间：")
@("* 修改描述：")
@("*************************************************/")
using @(Model[0].Namespace).Model;
using @(Model[0].Namespace).DotViewModel;

namespace @(Model[0].Namespace).Register
{
    public static class RegisterService
    {

        public static void AddServiceRegister(this IServiceCollection services)
        {
            @foreach (var item in Model)
	        {
                @("services.AddScoped<I"+item.TableName+"Service, "+item.TableName+"Service>();\t\f")
            }
        }

       @("-----------------------------------------AutoFac Ioc----------------------------------------")

        public static void RegisterService(this ContainerBuilder builder)
        {
              @foreach(var item in Model)
            {
                @("builder.RegisterType<" + item.TableName + "Service>().As<I" + item.TableName + "Service>().InstancePerLifetimeScope();\t\f")
            }
        }
}
}
