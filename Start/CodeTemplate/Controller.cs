﻿@("/***************************************************")
@("* 模块名称：" + Model.TableName + "Controller")
@("* 功能概述：")
@("* 创建人：何贵祥")
@("* 创建时间：" + DateTime.Now.ToString("d") + "")
@("* 修改人：")
@("* 修改时间：")
@("* 修改描述：")
@("*************************************************/")
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using @(Model.Namespace).CommonData.ResponseResult;
using @(Model.Namespace).CommonData.Comn;
using @(Model.Namespace).IService;
using @(Model.Namespace).Model;
using @(Model.Namespace).Filter.Login;
using System.Linq.Expressions;
using @(Model.Namespace).Common.ExpressionExtensions;
using @(Model.Namespace).CommonData.Page;
using @(Model.Namespace).ViewModel;
using @(Model.Namespace).Common.Helpers;
using @(Model.Namespace).Common.DistributedID;
using @(Model.Namespace).CommonData.ViewModel;
@using System.Collections.Generic;
@using MICZ.CodeBuilder.ViewModels;

namespace @(Model.Namespace).Controllers
{
    @("/// <summary>")
    @("/// "+Model.Description)
    @("/// </summary>")
    @{string _tablename = Model.TableName; string ServiceName = _tablename.First().ToString().ToLower() + _tablename.Substring(1); }
    @{ List<BuilderColumnViewModel> _Columns = Model.Columns; bool IsDel= _Columns.Where(a => a.ColumnName.ToLower() == "isdel").Count() > 0?true:false; 
    BuilderColumnViewModel IsDelColumn = _Columns.Where(a => a.ColumnName.ToLower() == "isdel").FirstOrDefault();
    var _Primarykey = _Columns.Where(asd => asd.ColumnIsPrimarykey == true).FirstOrDefault(); string PrimaryName = _Primarykey == null ? _Columns.FirstOrDefault().ColumnName : _Primarykey.ColumnName;
    string PrimarykeyType = _Primarykey == null ? "string" : _Primarykey.Type.ToLower();
    var _Identity = _Columns.Where(asd => asd.ColumnIsIdentity == true).FirstOrDefault(); string IdentityName = _Identity == null ? "" : _Identity.ColumnName;
    string IdentityType = _Identity == null ? "string" : _Identity.Type.ToLower();
    }
    public class @(Model.TableName)Controller : BaseController
    {
        private readonly I@(Model.TableName)Service @(ServiceName)Service;
        public @(Model.TableName)Controller(I@(Model.TableName)Service _@(ServiceName)Service)
        {
            @(ServiceName)Service = _@(ServiceName)Service;
        }
        #region 系统生成

        @("/// <summary>")
        @("/// "+ Model.Description + "获取单条信息")
        @("/// </summary>")
        @("/// <param name=\"ID\"></param>")
        @("/// <returns></returns>")
        [EnableCors]
        [ServiceFilter(typeof(LoginFilterAttribute))]
        [HttpGet("Get")]
        public async Task< IActionResult > Get(@(PrimarykeyType+"?") ID)
        {
            if (ID == null)
                return Content(ResponseResult.toJsonString(ResponseResult.Error(ReturnErrorCode.ParameterEmptyCode, ReturnErrorCode.ParameterEmpty)));
            @(Model.TableName) model = await @(ServiceName)Service.Get(a => a.@(PrimaryName) == ID);
            return Content(ResponseResult.toJsonString(ResponseResult.Success(model)));
        }

        @("/// <summary>")
        @("/// "+ Model.Description + "获取集合信息")
        @("/// </summary>")
        @("/// <param name=\"page\">页码</param>")
        @("/// <param name=\"rows\">数据量</param>")
        @("/// <returns></returns>")
        [EnableCors]
        [ServiceFilter(typeof(LoginFilterAttribute))]
        [HttpGet("GetList")]
        public async Task< IActionResult > GetList(int page = 0, int rows = 0)
        {
            if (page == 0 || rows == 0)
                return Content(ResponseResult.toJsonString(ResponseResult.Error(ReturnErrorCode.ParameterEmptyCode, ReturnErrorCode.ParameterEmpty)));
            Expression< Func< @(Model.TableName), bool>> expression = a => true;
            List< @(Model.TableName) > model = await @(ServiceName)Service.GetList(page, rows, "", expression);
            long Total = await @(ServiceName)Service.GetListTotal(expression);
            return Content(ResponseResult.toJsonString(ResponseResult.Success(new PageResult< @(Model.TableName) > (page, rows, Total, model))));
        }

        @("/// <summary>")
        @("/// "+ Model.Description + "新增")
        @("/// </summary>")
        @("/// <param name=\"model\"></param>")
        @("/// <returns></returns>")
        [EnableCors]
        [ServiceFilter(typeof(LoginFilterAttribute))]
        [HttpPost("Add")]
        public async Task< IActionResult > Add([FromBody] @(Model.TableName)ViewModel model)
        {
            if (model == null)
                return Content(ResponseResult.toJsonString(ResponseResult.Error(ReturnErrorCode.ParameterEmptyCode, ReturnErrorCode.ParameterEmpty)));
            @if(IsDel)
            { 
                if (IsDelColumn.Type.ToLower() == "bool")
                @("model." + IsDelColumn.ColumnName + " = false;")
                else
                @("model." + IsDelColumn.ColumnName + " = 0;")
            }
            model.CreateUserID = AuthContextUser.CurrentUser.UserID;
            model.CreateTime = DateTime.Now;
            @if(string.IsNullOrWhiteSpace(IdentityName)){ 
                @:model.@(PrimaryName) = IdGenerator.NextId().ToString();
                @:long @(PrimaryName) = await @(ServiceName)Service.Create(AutoMapperHelper.MapTo< @(Model.TableName) > (model));
                @:return Content(ResponseResult.toJsonString(ResponseResult.Success(model.@(PrimaryName))));
            }else{
                @:long @(PrimaryName) = await @(ServiceName)Service.Create(AutoMapperHelper.MapTo< @(Model.TableName) > (model), a => new { a.@(PrimaryName) });
                @:return Content(ResponseResult.toJsonString(ResponseResult.Success(@(PrimaryName))));
            }
        }

        @("/// <summary>")
        @("/// "+ Model.Description + "修改")
        @("/// </summary>")
        @("/// <param name=\"model\"></param>")
        @("/// <returns></returns>")
        [EnableCors]
        [ServiceFilter(typeof(LoginFilterAttribute))]
        [HttpPost("Edit")]
        public async Task< IActionResult > Edit([FromBody] @(Model.TableName)ViewModel model)
        {
            @if(string.IsNullOrWhiteSpace(IdentityName)){ 
                @:if (model == null || model.@(PrimaryName) == null|| string.IsNullOrWhiteSpace(model.@(PrimaryName)))
            }else { 
                @:if (model == null || model.@(PrimaryName) == null)
            }
            return Content(ResponseResult.toJsonString(ResponseResult.Error(ReturnErrorCode.ParameterEmptyCode, ReturnErrorCode.ParameterEmpty)));
            model.UpdateUserID = AuthContextUser.CurrentUser.UserID;
            model.UpdateTime = DateTime.Now;
            long cmd = await @(ServiceName)Service.Update(AutoMapperHelper.MapTo< @(Model.TableName) > (model), a => a.@(PrimaryName) == model.@(PrimaryName));
            return Content(ResponseResult.toJsonString(ResponseResult.Success(true)));
        }

        @if(IsDel)
        {
            @("/// <summary>")@:
            @("/// "+ Model.Description + "逻辑删除")@:
            @("/// </summary>")@:
            @("/// <param name=\"ID\">删除ID</param>")@:
            @("/// <returns></returns>")@:
            @:[EnableCors]
            @:[ServiceFilter(typeof(LoginFilterAttribute))]
            @:[HttpGet("SoftRemove")]
            @:public async Task< IActionResult > SoftRemove(@(PrimarykeyType+"?") ID)
            @:{
                @:if (ID == null)
                    @:return Content(ResponseResult.toJsonString(ResponseResult.Error(ReturnErrorCode.ParameterEmptyCode, ReturnErrorCode.ParameterEmpty)));
                @:@(Model.TableName) model = await @(ServiceName)Service.Get(a => a.@(PrimaryName) == ID);
                @:if (model == null)
                    @:return Content(ResponseResult.toJsonString(ResponseResult.Error(ReturnErrorCode.DataNotExistCode, ReturnErrorCode.DataNotExist)));
                @{
                if (IsDelColumn.Type.ToLower() == "bool")
                @("model." + IsDelColumn.ColumnName + " = true;")
                else
                @("model." + IsDelColumn.ColumnName + " = 1;")
                }
                @:model.UpdateUserID = AuthContextUser.CurrentUser.UserID;
                @:model.UpdateTime = DateTime.Now;
                @:
                @:int cmd = await @(ServiceName)Service.Update(model, a => new { a.@(PrimaryName) }, a => a.@(PrimaryName) == model.@(PrimaryName));
                @:return Content(ResponseResult.toJsonString(ResponseResult.Success(true)));

            @:}

            @("/// <summary>")@:
            @("/// "+ Model.Description + "恢复逻辑删除")@:
            @("///</summary>")@:
            @("/// <param name=\"ID\">恢复ID</param>")@:
            @("/// <returns></returns>")@:
            @:[EnableCors]
            @:[ServiceFilter(typeof(LoginFilterAttribute))]
            @:[HttpGet("CancelSoftRemove")]
            @:public async Task< IActionResult > CancelSoftRemove(@(PrimarykeyType+"?") ID)
            @:{

                @:if (ID == null)
                    @:return Content(ResponseResult.toJsonString(ResponseResult.Error(ReturnErrorCode.ParameterEmptyCode, ReturnErrorCode.ParameterEmpty)));
                @:@(Model.TableName) model = await @(ServiceName)Service.Get(a => a.@(PrimaryName) == ID, "", true);
                @:if (model == null)
                    @:return Content(ResponseResult.toJsonString(ResponseResult.Error(ReturnErrorCode.DataNotExistCode, ReturnErrorCode.DataNotExist)));
                @{
                if (IsDelColumn.Type.ToLower() == "bool")
                @("model." + IsDelColumn.ColumnName + " = false;")
                else
                @("model." + IsDelColumn.ColumnName + " = 0;")
                }
                @:model.UpdateUserID = AuthContextUser.CurrentUser.UserID;
                @:model.UpdateTime = DateTime.Now;
                @:
                @:int cmd = await @(ServiceName)Service.Update(model, a => new { a.@(PrimaryName) }, a => a.@(PrimaryName) == model.@(PrimaryName));
                @:return Content(ResponseResult.toJsonString(ResponseResult.Success(true)));
            @:}
        }

        @("/// <summary>")
        @("/// "+ Model.Description + "实体删除")
        @("/// </summary>")
        @("/// <param name=\"model\">实体</param>")
        @("/// <returns></returns>")
        [EnableCors]
        [ServiceFilter(typeof(LoginFilterAttribute))]
        [HttpGet("Remove")]
        public async Task< IActionResult > Remove(@(PrimarykeyType+"?") ID)
        {
            if (ID == null)
                return Content(ResponseResult.toJsonString(ResponseResult.Error(ReturnErrorCode.ParameterEmptyCode, ReturnErrorCode.ParameterEmpty)));
            long cmd = await @(ServiceName)Service.Remove(a => a.@(PrimaryName) == ID);
            return Content(ResponseResult.toJsonString(ResponseResult.Success(true)));
        }
        #endregion
    }
}
