﻿@("/***************************************************")
@("* 模块名称：Register")
@("* 功能概述：")
@("* 创建人：何贵祥")
@("* 创建时间：" + DateTime.Now.ToString("d") + "")
@("* 修改人：")
@("* 修改时间：")
@("* 修改描述：")
@("*************************************************/")
using @(Model[0].Namespace).Model;
using @(Model[0].Namespace).DotViewModel;

namespace @(Model[0].Namespace).CommonData.AutoMapper.Profiles
{
    public static class BaseConfigProfile : Profile
    {

        public BaseConfigProfile()
        {
            @foreach (var item in Model)
	        {
                @("CreateMap<" + item.TableName + ", " + item.TableName + "ViewModel>();\t\f");
                @("CreateMap<" + item.TableName + "ViewModel, " + item.TableName + ">();\t\f");
            }
        }
        
    }
}
