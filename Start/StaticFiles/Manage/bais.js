﻿var Base_url = "https://localhost:5000";

/**
 * 获取Token
 * 设置请求头*/
var token = localStorage.getItem("token");
var header = {};//{ "content-type": "application/json" };
var getheader = {"content-type": "application/json" };//未启用
var postheader = { "content-type": "application/x-www-form-urlencoded" };//未启用
if (token!=null&&token.trim() != "") {
    header.Authorization = "Bearer " + token;
    getheader.Authorization = "Bearer " + token;
    postheader.Authorization = "Bearer " + token;
}

//JS获取url参数
function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) { return pair[1]; }
    }
    return (false);
}